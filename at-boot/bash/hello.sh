#!/bin/bash
cat ./to_kill.txt | xargs kill -s KILL
/home/pi/pi_scripts/deployed/led-temp.py >> ~/log.txt 2>> ~/log_err.txt & 
id1=$!
/home/pi/pi_scripts/deployed/oled.py > /dev/null &
id2=$!
echo $id1 $id2 > ./to_kill.txt
