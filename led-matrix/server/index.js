const shell = require('shelljs');
const express = require('express')
var cors = require('cors')
const path = require('path');
const fs = require('fs');

const { networkInterfaces } = require('os');


const LED_MATRIX_SCRIPT = '/usr/local/bin/led-matrix';

const app = express()
app.use(express.json())
app.use(cors())
app.use(express.static(path.join(__dirname, 'public')));


function printOnLedMatrix(msg) {
    let command = `echo "${msg}" | ${LED_MATRIX_SCRIPT}`;
    console.log("command : ", command);
    let child = shell.exec(`sudo /bin/bash -c '${command}'`, { async: true });
    child.stdout.on('data', function (data) {
        console.log(" > " + data);
    });
}



app.get('/', function (req, res) {
    res.send('Hello World')
})

app.post('/api/printledmatrix', (req, res) => {
    console.log(req.body);
    let msg = req.body.msg;
    printOnLedMatrix(msg);
    res.status(200).send({ response: 'done' });
})
















var ips = []; // Or just '{}', an empty object
var interfaceAvailable = false;
do {
    console.log("chacking for network interfaces")
    let nets = networkInterfaces();
    for (let name of Object.keys(nets)) {
        for (let net of nets[name]) {
            // Skip over non-IPv4 and internal (i.e. 127.0.0.1) addresses
            if (net.family === 'IPv4' && !net.internal) {
                ips.push(net.address);
                interfaceAvailable = true;
            }
        }
    }
    if (!interfaceAvailable) {
        console.log("no network interfacea are available, checking again in 3s ...")
        let timer = ms => new Promise(res => setTimeout(res, ms));
        console.log("wait 3 seconds")
        timer(3000).then(_ => console.log(""));
    }else{
        console.log("found: ", ips)
    }
} while (!interfaceAvailable)






const server = app.listen(8080, () => {
    // TODO: prioritize ip from ethernet if available
    let ipAndPort = ips[0] + ":" + server.address().port;
    let config = { ip: "http://" + ipAndPort };
    console.log("page config = ", config);
    var saveJson = JSON.stringify(config, null, 4)

    fs.writeFile('./public/config.json', saveJson, 'utf8', (err) => {
        if (err) {
            console.log(err)
        }
        console.log("saved config")
        console.log(config)
    });

    printOnLedMatrix(ipAndPort);



})