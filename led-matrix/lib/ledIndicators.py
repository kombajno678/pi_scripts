#!/usr/bin/env python3
import time
from time import sleep
import RPi.GPIO as GPIO


class Indicators:
    pwnFreq = 200

    def __init__(self):
        self.leds = {}
        GPIO.setmode(GPIO.BCM)
    
    def add(self, name, pin):
        self.leds[name] = pin
        GPIO.setup(pin, GPIO.OUT)
        self.off(name)
    
    def on(self, name=None):
        if name:
            GPIO.output(self.leds[name], 1)
        else:
            for key, val in self.leds.items():
                GPIO.output(val, 1)
    
    def off(self, name=None):
        if name:
            GPIO.output(self.leds[name], 0)
        else:
            for key, val in self.leds.items():
                GPIO.output(val, 0)

    def pwm(self, name=None, fill=100, sleeptime=1):
        if fill < 0: 
            fill = 0
        if fill > 100:
            fill = 100

        if name:
            pin = self.leds[name]
            myPWM = GPIO.PWM(pin, Indicators.pwnFreq)
            myPWM.start(fill)
            myPWM.ChangeDutyCycle(fill)
            sleep(sleeptime)
            myPWM.stop()
        else:
            for key, val in self.leds.items():
                pin = val
                myPWM = GPIO.PWM(pin, Indicators.pwnFreq)
                myPWM.start(fill)
                myPWM.ChangeDutyCycle(fill)
                sleep(sleeptime)
                myPWM.stop()
        
    def setState(self, on=None, name=None):
        if on is not None:
            if on:
                self.on(name)
            else:
                self.off(name)


    def flash(self):
        for key, val in self.leds.items():
            GPIO.output(val, 1)
            sleep(0.1)
        for key, val in self.leds.items():
            GPIO.output(val, 0)
            sleep(0.1)
    
    def blink(self, sleeptime=0.5, times=1, name=None):
        if name:
            for i in range(times):
                GPIO.output(self.leds[name], 0)
                GPIO.output(self.leds[name], 1)
                sleep(sleeptime)
                GPIO.output(self.leds[name], 0)
                sleep(sleeptime)
        else:
            for i in range(times):
                for key, val in self.leds.items():
                    GPIO.output(val, 0)
                    GPIO.output(val, 1)
                sleep(sleeptime)
                for key, val in self.leds.items():
                    GPIO.output(val, 0)
                sleep(sleeptime)


    def cleanup(self):
        print("cleanup")
        GPIO.cleanup()

    def __del__(self):
        print("__del__")
        self.cleanup()

class LedIndicators(Indicators):
    ledPins = {
        "r": 4,
        "g": 14,
        "b": 15,
        "y": 17
    }
    # redPin = 4
    # greenPin = 14
    # bluePin = 15
    # yellowPin = 17
    def __init__(self, name=None):
        super().__init__()
        if name:
            self.add(name, LedIndicators.ledPins[name])
        else:
            for key, val in LedIndicators.ledPins.items():
                self.add(key, val)

def led_control_test():
    leds = LedIndicators("g")
    leds.blink(sleeptime=0.1, times=10)


if __name__ == "__main__":
    led_control_test()
