
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (c) 2017-18 Richard Hull and contributors
# See LICENSE.rst for details.

# importnat, dont move or rename

# https://luma-led-matrix.readthedocs.io/en/latest/install.html
# https://pypi.org/project/luma.led_matrix/

# DIN 19 GPIO10
# CS 24 GPIO8
# CLK 23 GPIO11

import re
import time
# import argparse
import sys

from luma.led_matrix.device import max7219
from luma.core.interface.serial import spi, noop
from luma.core.render import canvas
# from luma.core.virtual import viewport
from luma.core.legacy import text, show_message
from luma.core.legacy.font import proportional, ATARI_FONT, SEG7_FONT, CP437_FONT, TINY_FONT, SINCLAIR_FONT, LCD_FONT

# Number of cascaded MAX7219 LED matrices
block_orientation = 0 

#Corrects block orientation when wired vertically
cascaded=1

#Rotate display 0=0°, 1=90°, 2=180°, 3=270°
rotate=1

#Set to true if blocks are in reverse order
inreverse=False

msg = "???"
serial = spi(port=0, device=0, gpio=noop())
device = max7219(serial, cascaded=cascaded, block_orientation=block_orientation,
                    rotate=rotate, blocks_arranged_in_reverse_order=inreverse)
device.contrast(0)
for line in sys.stdin:
    if 'q' == line.rstrip():
        break
    
    #show_message(device, line, fill="white", font=proportional(TINY_FONT), scroll_delay=0.13)
    virtual = viewport(device, width=device.width, height=8)
    with canvas(virtual) as draw:
        text(draw, (0, i * 8), line, fill="white", font=proportional(TINY_FONT))




# for i in range(virtual.height - device.height):
#     virtual.set_position((0, i))
#     time.sleep(0.05)