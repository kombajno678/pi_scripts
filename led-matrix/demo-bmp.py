import time
from luma.core.render import canvas
from luma.core import legacy

# http://dotmatrixtool.com/# 
# 8px by 8px, column major, little endian

def get_device():
    from luma.led_matrix.device import max7219
    from luma.core.interface.serial import spi, noop
    block_orientation = 0 
    cascaded=1
    rotate=1
    inreverse=False
    serial = spi(port=0, device=0, gpio=noop())
    device = max7219(serial, cascaded=cascaded, block_orientation=block_orientation,
                    rotate=rotate, blocks_arranged_in_reverse_order=inreverse)
    return device

def main():
    MY_CUSTOM_BITMAP_FONT = [
        [
            0x1c, 0x3e, 0x7f, 0xfe, 0xfe, 0x7f, 0x3e, 0x1c
        ]
    ]


    device = get_device()
    with canvas(device) as draw:
        # Note that "\0" is the zero-th character in the font (i.e the only one)
        legacy.text(draw, (0, 0), "\0", fill="white", font=MY_CUSTOM_BITMAP_FONT)

    time.sleep(5)


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        pass