#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (c) 2017-18 Richard Hull and contributors
# See LICENSE.rst for details.

# importnat, dont move or rename

# https://luma-led-matrix.readthedocs.io/en/latest/install.html
# https://pypi.org/project/luma.led_matrix/

# DIN 19 GPIO10
# CS 24 GPIO8
# CLK 23 GPIO11

import re
from time import sleep
import argparse
import sys

from luma.led_matrix.device import max7219
from luma.core.interface.serial import spi, noop
from luma.core.render import canvas
# from luma.core.virtual import viewport
from luma.core.legacy import text, show_message
from luma.core.legacy.font import proportional, TINY_FONT, CP437_FONT, SINCLAIR_FONT, LCD_FONT
from luma.core.virtual import viewport


parser = argparse.ArgumentParser(description='led-matrix arguments',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--bright', '-b', type=int, default=127, help='brightness (0-255)')
parser.add_argument('--rotate', '-r', type=int, default=1, choices=[0, 1, 2, 3], help='Rotate display 0=0°, 1=90°, 2=180°, 3=270°')
parser.add_argument('--font', '-f', type=int, default=0, choices=[0, 1, 2, 3], help='fonts to choose from: 0 - TINY_FONT, 1 - CP437_FONT, 2 - SINCLAIR_FONT, 3 - LCD_FONT')
parser.add_argument('--message', '-m', type=str, help='message to display, overrides message from stdin')

parser.add_argument('--speed', '-s', dest='speed', type=int, default=0, choices=[0, 1, 2, 3], help='only without --static, speed of scroll')

parser.add_argument('--static', dest='static', action='store_true', help='displays text without scrolling, useful for displaying two digit numbers')
parser.add_argument('--no-static', dest='static', action='store_false', help='default behaviour, will scroll text from left to right')
parser.set_defaults(static=False)

parser.add_argument('--time', '-t', type=int, default=1, help='only with --static, for how many seconds will text be displayed (default is 1s)')


args = parser.parse_args()

if args.bright > 255:
    args.bright = 255
if args.bright < 0:
    args.bright = 0


# Number of cascaded MAX7219 LED matrices
block_orientation = -90 
#Corrects block orientation when wired vertically
cascaded=4
#Rotate display 0=0°, 1=90°, 2=180°, 3=270°
rotate=0
#Set to true if blocks are in reverse order
inreverse=False

serial = spi(port=0, device=0, gpio=noop())
device = max7219(serial, cascaded=cascaded, block_orientation=block_orientation,
                    rotate=rotate, blocks_arranged_in_reverse_order=inreverse)
device.contrast(args.bright)



def ledPrint(msg):
    font = LCD_FONT

    if(args.static):
        virtual = viewport(device, width=device.width, height=8)
        with canvas(virtual) as draw:
            text(draw, (0, 0), msg, fill="white", font=proportional(font))
        sleep(args.time)
    else:
        if args.speed == 0:
            scroll_delay = 0.2
        if args.speed == 1:
            scroll_delay = 0.13
        if args.speed == 2:
            scroll_delay = 0.10
        if args.speed == 3:
            scroll_delay = 0.07

        scroll_delay = 0.07
        show_message(device, msg, fill="white", font=proportional(font), scroll_delay=scroll_delay)



if args.message:
    msg = args.message
    ledPrint(msg)
else:
    for line in sys.stdin:
        line = line.replace("\n", "").rstrip()
        if 'q' == line:
            break
        ledPrint(line)


