import sys
import random
from time import sleep

import luma.core.render
from luma.core.sprite_system import framerate_regulator

def get_device():
    # create matrix device
    from luma.led_matrix.device import max7219
    from luma.core.interface.serial import spi, noop
    
    # Number of cascaded MAX7219 LED matrices
    block_orientation = 0 

    #Corrects block orientation when wired vertically
    cascaded=1

    #Rotate display 0=0°, 1=90°, 2=180°, 3=270°
    rotate=1

    #Set to true if blocks are in reverse order
    inreverse=False

    serial = spi(port=0, device=0, gpio=noop())
    device = max7219(serial, cascaded=cascaded, block_orientation=block_orientation,
                    rotate=rotate, blocks_arranged_in_reverse_order=inreverse)

    return device


class Ball(object):
    def __init__(self, w, h, radius, color):
        self._w = w
        self._h = h
        self._radius = radius
        self._color = color
        self._x_speed = (random.random() - 0.5) * 1
        self._y_speed = (random.random() - 0.5) * 1
        self._x_pos = self._w / 2.0
        self._y_pos = self._h / 2.0

    def update_pos(self):
        if self._x_pos + self._radius > self._w:
            self._x_speed = -abs(self._x_speed)
        elif self._x_pos - self._radius < 0.0:
            self._x_speed = abs(self._x_speed)

        if self._y_pos + self._radius > self._h:
            self._y_speed = -abs(self._y_speed)
        elif self._y_pos - self._radius < 0.0:
            self._y_speed = abs(self._y_speed)

        self._x_pos += self._x_speed
        self._y_pos += self._y_speed

    def draw(self, canvas):
        canvas.ellipse((self._x_pos - self._radius, self._y_pos - self._radius,
                       self._x_pos + self._radius, self._y_pos + self._radius), fill=self._color)


def main(num_iterations=sys.maxsize):
    colors = ["white"]
    #balls = [Ball(device.width/2, device.height/2, i * 1.5, "white") for i in range(10)]
    balls = [Ball(device.width, device.height, 0, "white") for i in range(3)]

    frame_count = 0
    fps = ""
    canvas = luma.core.render.canvas(device)

    regulator = framerate_regulator(fps=0)

    while num_iterations > 0:
        with regulator:
            num_iterations -= 1

            frame_count += 1
            with canvas as c:
                c.rectangle(device.bounding_box, outline="black", fill="black")
                for b in balls:
                    b.update_pos()
                    b.draw(c)
                # c.text((2, 0), fps, fill="white")

            if frame_count % 20 == 0:
                fps = "FPS: {0:0.3f}".format(regulator.effective_FPS())
                print(f"fps = {fps}")
            sleep(0.05)


if __name__ == '__main__':
    try:
        device = get_device()
        main()
    except KeyboardInterrupt:
        pass