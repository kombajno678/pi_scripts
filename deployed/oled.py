#!/usr/bin/env python3
import os
import psutil
import signal
flip = False
timePassed = 0

temp = 0
cpu  = 0
ram = 0
ip = '...'

def getCPUtemperature():
    return os.popen('vcgencmd measure_temp').readline().replace("temp=","").replace("'C\n","")

def getCpuUsage():
    return psutil.cpu_percent()

def getRamUsage():
    return psutil.virtual_memory().percent

def getIp():
    return os.popen("hostname -I").readline()

def getUptime():
    return os.popen("uptime -p").readline().replace("minutes","m").replace("hours","h").replace("days","d")
    


# Copyright (c) 2017 Adafruit Industries
# Author: Tony DiCola & James DeVito
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
import time
# import Adafruit_GPIO.SPI as SPI
import Adafruit_SSD1306

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont


# Raspberry Pi pin configuration:
RST = None     # on the PiOLED this pin isnt used
# Note the following are only used with SPI:
# DC = 23
# SPI_PORT = 0
# SPI_DEVICE = 0


# 128x32 display with hardware I2C:
disp = Adafruit_SSD1306.SSD1306_128_32(rst=RST)

# Initialize library.
disp.begin()
# Clear display.
disp.clear()
disp.display()
disp.set_contrast(0)
# Create blank image for drawing.
# Make sure to create image with mode '1' for 1-bit color.
width = disp.width
height = disp.height
image = Image.new('1', (width, height))

# Get drawing object to draw on image.
draw = ImageDraw.Draw(image)

# Draw a black filled box to clear the image.
draw.rectangle((0,0,width,height), outline=0, fill=0)

# Draw some shapes.
# First define some constants to allow easy resizing of shapes.
padding = -2
top = padding
bottom = height-padding
# Move left to right keeping track of the current x position for drawing shapes.
x = 0


# Load default font.
font = ImageFont.load_default()

# Alternatively load a TTF font.  Make sure the .ttf font file is in the same directory as the python script!
# Some other nice fonts to try: http://www.dafont.com/bitmap.php
# font = ImageFont.truetype('Minecraftia.ttf', 8)



animation_x = 0
animation_d = 4
framesPassed = 0
cpu = getCpuUsage()
ram = getRamUsage()
temp = getCPUtemperature()
ip = getIp()
blink = 0
ipIndex = 0


def gracefullyExit(*args):
    draw.rectangle((0,0,width,height), outline=0, fill=0)
    draw.text((x, top),    "\    /\\", ffont=font, fill=255)
    draw.text((x, top+8),  " )  ( ')", ffont=font, fill=255)
    draw.text((x, top+16), "(  /  )", font=font, fill=255)
    draw.text((x, top+25), " \(__)|", font=font, fill=255)
    disp.image(image)
    disp.display()



signal.signal(signal.SIGINT, gracefullyExit)
signal.signal(signal.SIGTERM, gracefullyExit)

try:
    while True:
        # Draw a black filled box to clear the image.
        draw.rectangle((0,0,width,height), outline=0, fill=0)

        cpu = getCpuUsage()
        ram = getRamUsage()
        temp = getCPUtemperature()
        ip = getIp().split()
        ip = list(filter(lambda addr: not addr.startswith("172"), ip))
        if len(ip) == 0:
            ip.append("(none)")
        ipIndex += 1
        if ipIndex >= len(ip):
            ipIndex = 0
            
        uptime = str(getUptime())
        #timePassed = 0
        draw.text((x, top),       f"{ip[ipIndex]} [{ipIndex+1}/{len(ip)}]",  font=font, fill=255)
        draw.text((x, top+8),     f"cpu {cpu:3.0f} tmp {float(temp):2.0f} ram {ram:2.0f}", font=font, fill=255)
        draw.text((x, top+16),    uptime,  font=font, fill=255)
        draw.text((x, top+25),    "."*blink,  font=font, fill=255)
        # Display image.
        
        blink += 1
        if blink > 12:
            blink = 0
        if flip:
            disp.image(image.transpose(Image.ROTATE_180))
        else:
            disp.image(image)
        disp.display()
        time.sleep(3)
except KeyboardInterrupt:
    print("handling KeyboardInterrupt")
    gracefullyExit()



