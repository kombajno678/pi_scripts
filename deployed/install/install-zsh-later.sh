#!/bin/bash
set -xe
rm -rf ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
git clone git://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
rm -rf ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
sudo sed -i 's/plugins=(git)/plugins=(git zsh-autosuggestions zsh-syntax-highlighting)/g' ~/.zshrc

#echo "please install fonts from : https://github.com/romkatv/powerlevel10k#meslo-nerd-font-patched-for-powerlevel10k"
#sudo rm -rf /usr/share/fonts/truetype/meslo && sudo mkdir -p /usr/share/fonts/truetype/meslo && sudo chmod 755 /usr/share/fonts/truetype/meslo
# fontsFolder="/usr/share/fonts/truetype/meslo/"
#fontsFolder="~/.fonts/"
#sudo curl "https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf" -o "${fontsFolder}MesloLGS_NF_Regular.ttf" --create-dirs 
#sudo curl "https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold.ttf" -o "${fontsFolder}MesloLGS_NF_Bold.ttf" --create-dirs
#sudo curl "https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Italic.ttf" -o "${fontsFolder}MesloLGS_NF_Italic.ttf" --create-dirs
#sudo curl "https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold%20Italic.ttf" -o "${fontsFolder}MesloLGS_NF_Bold_Italic.ttf" --create-dirs
#sudo curl "https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/DejaVuSansMono/Regular/complete/DejaVu%20Sans%20Mono%20Nerd%20Font%20Complete.ttf" -o "${fontsFolder}DejaVu_Sans_Mono_Nerd_Font_Complete.ttf" --create-dirs
#sudo fc-cache -rsf


#echo "looks like fonts not working so fuck"
# https://github.com/romkatv/powerlevel10k#meslo-nerd-font-patched-for-powerlevel10k
rm -rf ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
sudo sed -i 's/ZSH_THEME="robbyrussell"/ZSH_THEME="powerlevel10k\/powerlevel10k"/g' ~/.zshrc

echo "now run : p10k configure"
