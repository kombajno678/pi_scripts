#!/bin/bash
sudo ln -sf /bin/python3 /bin/python

# led matrix setup

usermod -a -G spi,gpio pi
sudo easy_install -U pip
sudo -H pip install --upgrade --ignore-installed pip setuptools
sudo apt install build-essential python3-dev python3-pip libfreetype6-dev libjpeg-dev libopenjp2-7 libtiff5
sudo -H pip install --upgrade luma.led_matrix
sudo ln -s /home/pi/pi_scripts/deployed/ledm.py  /usr/local/bin/led-matrix

# shutdown button
echo "dtoverlay=gpio.shutdown" >> /boot/config.txt

# fan control
sudo -H pip install pigpio
sudo echo -ne '#!/bin/bash\nexport FAN_PIN=13\nexport FAN_TEMP=55' > /etc/profile.d/setup-envs.sh # might require manual editing
sudo ln -s /home/pi/pi_scripts/deployed/fan-control.py  /usr/local/bin/fan-control


# for execution script at boot:
# sudo crontab -e -> add this line: @reboot /bin/bash -c 'for f in /home/pi/pi_scripts/at-boot/bash/*.sh; do sudo /bin/bash "$f"; done'
# @reboot /bin/bash -c '/home/pi/pi_scripts/at-boot/bash/hello.sh'

# stresstest:
export PATH=$PATH:/home/pi/.local/bin # maybe add to .bashrc
sudo apt install stress
pip install stressberry --user
# stressberry-run out.dat
# stressberry-plot out.dat -o out.png



# oled
pip install Adafruit_SSD1306
