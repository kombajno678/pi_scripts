#!/bin/bash
#log="temp-log.txt"
#echo "" > "$log"

good="\e[32m"
danger="\e[33m"
bad="\e[31m"
clear="\e[0m"

max=0
avg=0

while [ true ]; do
	clear
	temp=$(vcgencmd measure_temp | cut -d '=' -f 2)
	temp=${temp:0:4}
	freq=$(vcgencmd measure_clock arm | cut -d '=' -f 2)
	freq=$(echo "scale=2; $freq / 1000000" | bc)
	
	temp_int=${temp:0:2}
	color=""
	if [ "$temp_int" -le 60 ]; then
		color=$good
	elif [ "$temp_int" -le 70 ]; then
		color=$danger
	else
		color=$bad
	fi
	
	if [ "$temp_int" -gt "$max" ]; then
		max=$temp_int
	fi

	echo -e "{ \"temp\" : "$color $temp $clear"; \"max\" : $max ; \"freq\" : "$freq"; }\e[0m"
	#echo $temp >> "$log"
	sleep 5
done
