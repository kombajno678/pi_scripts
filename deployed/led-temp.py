#!/usr/bin/env python3

import math
import time
import argparse
import random
import os
from luma.led_matrix.device import max7219
from luma.core.interface.serial import spi, noop
from luma.core.render import canvas
# from luma.core.virtual import viewport
from luma.core.legacy import text, show_message
from luma.core.legacy.font import proportional, TINY_FONT, CP437_FONT, SINCLAIR_FONT, LCD_FONT
from luma.core.virtual import viewport
from luma.core.sprite_system import framerate_regulator
import psutil
import datetime
from pathlib import Path
from PIL import ImageFont
import random
import signal

import requests

weather_temp = "??"

CURRENT_WEATHER_URL = 'http://api.weatherapi.com/v1/current.json?key=188d3d8e6463412da4e223835212712&q=Lodz&aqi=no'

def getCurrentWeather():
    try:
        r = requests.get(CURRENT_WEATHER_URL)
        return r.json(), r.status_code
    except Exception:
        return {}, -1

def getCPUtemperature(self):
    res = os.popen('vcgencmd measure_temp').readline()
    temp =(res.replace("temp=","").replace("'C\n",""))
    return temp

def getCpuUsage(self):
    return psutil.cpu_percent()

def getRamUsage(self):
    return psutil.virtual_memory().percent

font_paths = [
    "arcade-classic.ttf",
    "brixel-8x8-mono.ttf",
    "ds-firmware-clock.ttf",
    "family-basic.ttf",
    "C&C Red Alert [INET].ttf",
    "ChiKareGo.ttf",
    "code2000.ttf",
    "fontawesome-webfont.ttf",
    "FreePixel.ttf",
    "miscfs_.ttf",
    "pixelmix.ttf",
    "ProggyTiny.ttf",
    "tiny.ttf",
    "Volter__28Goldfish_29.ttf",
    "creep.bdf"
]


def make_font(name, size):
    font_path = str(Path(__file__).resolve().parent.joinpath('fonts', name))
    return ImageFont.truetype(font_path, size)


def infinite_shuffle(arr):
    copy = list(arr)
    while True:
        random.shuffle(copy)
        for elem in copy:
            yield elem


parser = argparse.ArgumentParser(description='led-matrix arguments',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--bright', '-b', type=int, default=0, help='brightness (0-255)')
parser.add_argument('--rotate', '-r', type=int, default=1, choices=[0, 1, 2, 3], help='Rotate display 0=0°, 1=90°, 2=180°, 3=270°')
parser.add_argument('--font', '-f', type=int, default=1, choices=[*range(0, len(font_paths))], help=f'font number, from 0 to {len(font_paths)}')
parser.add_argument('--size', '-s', type=int, default=8, help=f'font size')

args = parser.parse_args()

if args.bright > 255:
    args.bright = 255
if args.bright < 0:
    args.bright = 0


block_orientation = -90
cascaded=8
rotate=0
inreverse=False


# --display max7219 -i spi --width 32 --height 16 --block-orientation -90

class Display():
    
    def __init__(self, bright):
        self.legacy_fonts = [TINY_FONT, CP437_FONT, SINCLAIR_FONT, LCD_FONT]
        self.bright = bright
        #self.font = make_font("fontawesome-webfont.ttf", self.device.height - 2)
        font_path = str(Path(__file__).resolve().parent.joinpath('fonts', font_paths[args.font]))
        self.font = ImageFont.truetype(font_path, args.size)
        self.fontSmaller = proportional(TINY_FONT)
        # self.fontSmaller = ImageFont.truetype(str(Path(__file__).resolve().parent.joinpath('fonts', font_paths[2])), 2)

        self.regulator = framerate_regulator(fps=15)

        self.serial = spi(port=0, device=0, gpio=noop())
        self.device = max7219(self.serial, width=32, height=16, block_orientation=block_orientation,
                            rotate=rotate, blocks_arranged_in_reverse_order=inreverse)
        self.device.contrast(self.bright)
        self.snow = []
        self.snowCount = 30

        self.prev_minutes = -1
        self.minutes = 0
        self.weather = {}
        self.weather_status_code = 0
        
        self.timeStart = time.time()
        self.lastFrameTime = time.time()
        self.timeDelta = 0
        
    def getElapsedTime(self):
        return time.time() - self.timeStart
    
    def updateTimeDelta(self):
        self.timeDelta = time.time() - self.lastFrameTime
        self.lastFrameTime = time.time()
        
        
    def drawSnow(self, draw):
        for s in self.snow:
            # x = s[0]
            # y = s[1]+8
            draw.rectangle((s[0], s[1]+8, s[0], s[1]+8), fill="white")
            
    def moveSnow(self):
        for snowflake in self.snow:
            snowflake[0] += random.choice([-1, 0, 0, 0, 0,1])
            snowflake[1] += random.choice([0, 1])
        self.snow = list(filter(lambda s : s[1] < 8 and s[0] < 32 and s[1] >= 0 and s[0] >= 0, self.snow))

    def generateSnow(self):
        while len(self.snow) < self.snowCount:
            self.snow.append([random.randint(0, 29), random.randint(0, 2)])

    def clear(self):
        virtual = viewport(self.device, width=self.device.width, height=self.device.height)
        with canvas(self.device) as draw:
            draw.rectangle((0, 0, self.device.width, self.device.height), fill="black")

    def _drawCmd(self, draw):
        #msg = os.popen('vcgencmd measure_temp').readline()
        msg = os.popen('uptime').readline()
        w, h = draw.textsize(text=msg, font=(self.font))
        if w > self.device.width:
            #scroll
            offset = (self.regulator.called*2) % w
            text(draw, (self.device.width-offset,8), msg, fill="white", font=proportional(TINY_FONT))
        else:
            text(draw, (16,8), msg, fill="white", font=proportional(LCD_FONT))

    def _drawShit(self, draw):
        try:
            txt1 = f"{round(self.weather['current']['temp_c'])}/{round(self.weather['current']['feelslike_c'])}C"
        except Exception:
            txt1 = f"{self.weather_status_code}"
        text(draw, (0, 7+1), txt1, fill="white", font=(TINY_FONT))
        
        
    def drawWave(self, draw):
        fill = False
        frequency = 0.3
        phase = self.getElapsedTime()*2
        x = [i for i in range(32)]
        y = [math.sin(i*frequency + phase) for i in x]
        
        for X,Y in zip(x, y):
            Y = (4*Y)+12
            Y_ = 15 if fill else Y
            draw.rectangle((X, Y, X+1, Y_), fill="white")
        
        fill = False
        frequency = 0.3
        phase = self.getElapsedTime()*-2
        y = [math.sin(i*frequency + phase) for i in x]
        for X,Y in zip(x, y):
            Y = (4*Y)+12
            Y_ = 15 if fill else Y
            draw.rectangle((X, Y+1, X, Y_), fill="white")


            

    def _drawTime(self, draw):
        self.hours = datetime.datetime.now().strftime("%H")
        self.minutes = datetime.datetime.now().strftime("%M")
        self.seconds = datetime.datetime.now().strftime("%S")
        sep = ":"# if self.regulator.called % 2 == 0 else " "
        draw.text((0,0), text=self.hours, font=(self.font), fill="white")
        draw.text((16+1,0), text=self.minutes, font=(self.font), fill="white")
        draw.text((15-2,0), text=sep, font=(self.font), fill="white")

        
        


    def run(self):
        while True:
            with self.regulator:
                self.updateTimeDelta()
                virtual = viewport(self.device, width=self.device.width, height=self.device.height)
                with canvas(virtual) as draw:
                    draw.rectangle((0, 0, self.device.width, self.device.height), fill="black")

                    self._drawTime(draw)
                    self.drawWave(draw)
                    # if(self.prev_minutes != self.minutes):
                    #     self.weather, self.weather_status_code = getCurrentWeather()
                    # self.prev_minutes = self.minutes
                    # self._drawShit(draw)
                    


    def __del__(self, *args):
        try:
            self.clear()
        except Exception as e:
            pass

def handleSignal(a, b):
    display.__del__()
    quit()

display = None
if __name__ == "__main__":
    display = Display(args.bright)

    signal.signal(signal.SIGINT, handleSignal)
    signal.signal(signal.SIGTERM, handleSignal)
    display.run()
    # try:
    #     display.run()
    # except Exception as e:
    #     display.__del__()
    
