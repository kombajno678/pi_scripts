#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Author: Andreas Spiess
import os
import time
from time import sleep
import signal
import sys
import RPi.GPIO as GPIO

import pigpio 
import argparse

import lib.ledIndicators as led




parser = argparse.ArgumentParser(description='led-matrix arguments',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--fan-speed', '-f', dest='fanspeed', type=int, help='fan speed % (0-100)')
args = parser.parse_args()
forcedFanSpeed = args.fanspeed
if forcedFanSpeed:
    print(f"forcedFanSpeed = {forcedFanSpeed}")


try:
    fanPin = int(os.environ['FAN_PIN'])  # The pin ID, edit here to change it
except:
    fanPin = 13




fanSpeed=100
sum=0
pTemp=15
iTemp=0.4
myPWM=None

def getCPUtemperature():
    res = os.popen('vcgencmd measure_temp').readline()
    temp =(res.replace("temp=","").replace("'C\n",""))
    return temp

def handleFanStupid(pin, desiredTemp):
    global fanSpeed
    handleFanStupid.sumDiff = 0
    actualTemp = float(getCPUtemperature())

    diff=actualTemp-desiredTemp
    handleFanStupid.sumDiff=handleFanStupid.sumDiff+diff
    pDiff=diff*pTemp
    iDiff=handleFanStupid.sumDiff*iTemp
    fanSpeed=pDiff + iDiff
    
    if fanSpeed > 100:
        fanSpeed = 100
    if fanSpeed <= 0:
        fanSpeed = 0

    if handleFanStupid.sumDiff > 100:
        handleFanStupid.sumDiff = 100
    if handleFanStupid.sumDiff < -100:
        handleFanStupid.sumDiff = -100

    #print(f"p={pDiff}, i={iDiff}, d={diff}, sumDiff={handleFanStupid.sumDiff},    fanSpeed = {fanSpeed}")
    if fanSpeed >= 70:
        GPIO.output(pin, GPIO.HIGH)
    if fanSpeed <= 20:
        GPIO.output(pin, GPIO.LOW)

    # if diff >= 5:
    #     GPIO.output(pin, 1)
    # if diff <= 5:
    #     GPIO.output(pin, 0)

    # print(f"actualTemp = {actualTemp}")
    # print(f"diff = {diff}")

    print("actualTemp %4.2f TempDiff %4.2f ;;;  fanSpeed %5d" % (actualTemp,diff, fanSpeed))

def handleFanPwm(fanPwm=None, led=None, desiredTemp=None):
    global fanSpeed
    actualTemp = float(getCPUtemperature())
    diff=actualTemp-desiredTemp
    sumDiff=handleFanPwm.sumDiff+diff
    pDiff=diff*pTemp
    iDiff=sumDiff*iTemp
    fanSpeed=pDiff + iDiff
    
    if handleFanPwm.counter == 0:
        if fanSpeed > 70:
            fanPwm.ChangeDutyCycle(100)
            led.setState(True)
        if fanSpeed <= 10:
            fanPwm.ChangeDutyCycle(0)
            led.setState(False)
        print("actualTemp %4.2f TempDiff %4.2f fanSpeed %5d, counter %d" % (actualTemp,diff, fanSpeed, handleFanPwm.counter))

    
    
    if sumDiff > 100:
        sumDiff = 100
    if sumDiff < -100:
        sumDiff = -100
    #print(f"p={pDiff}, i={iDiff}, d={diff}, sumDiff={sumDiff},    fanSpeed = {fanSpeed}")
    #pwm.ChangeDutyCycle(fanSpeed)
    
    handleFanPwm.counter = handleFanPwm.counter + 1
    if handleFanPwm.counter >= 10:
        handleFanPwm.counter = 0
    handleFanPwm.sumDiff = sumDiff
    
        

handleFanPwm.sumDiff= 0
handleFanPwm.counter = 0


def exit_gracefully(pwm):
    pwm.ChangeDutyCycle(100)
    sleep(1)
    pwm.stop()
    sleep(1) # won't stop fan without sleep


def getDesiredTemp():
    try:
        desiredTemp = int(os.environ['FAN_TEMP']) #50 # The maximum temperature in Celsius after which we trigger the fan
        if desiredTemp > 70:
            desiredTemp = 70
    except:
        desiredTemp = 70
    return desiredTemp

def new_main():
    try:
        #GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(fanPin, GPIO.OUT)
        myPWM=GPIO.PWM(fanPin,200)
        exit_gracefully.pwm =myPWM
        myPWM.start(100)
        myPWM.ChangeDutyCycle(100)
        fanLed = led.LedIndicators("y")
        fanLed.blink(sleeptime=0.1, times=5)
        while True:
            if forcedFanSpeed:
                myPWM.ChangeDutyCycle(100)
                fanLed.on()
                #GPIO.output(fanPin, GPIO.HIGH)
            else:
                handleFanPwm(fanPwm=myPWM, led=fanLed, desiredTemp=getDesiredTemp())
                sleep(1) # Read the temperature every 5 sec, increase or decrease this limit if you want 
    except Exception as e: # KeyboardInterrupt: # trap a CTRL+C keyboard interrupt
        print(e)
        if myPWM:
            exit_gracefully(myPWM)
        GPIO.cleanup()
    

new_main()
